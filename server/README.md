

Installation

1. Need Nodejs
Look and see what the most recent version of node is and replace the version used below with the path to it. You can browse http://nodejs.org/dist to find the latest and then look for the *-arm-pi.tar.gz file within that folder; 0.11.9 was the latest when I installed.

Perform:
  wget http://nodejs.org/dist/v0.11.9/node-v0.11.9-linux-arm-pi.tar.gz
  tar -xvzf node-v0.11.9-linux-arm-pi.tar.gz
  echo 'NODE_PATH=$HOME/node-v0.11.9-linux-arm-pi/bin' >> ~/.bashrc
  echo '$PATH=$PATH:$NODE_PATH' >> ~/.bashrc
  source ~/.bashrc

2. Need youtube-dl
Perform:
  sudo apt-get install youtube-dl

3. Need 'Web' browser
Add new line to /etc/apt/sources.list:
  deb http://raspberrypi.collabora.com wheezy web
Perform:
  sudo apt-get update 
  sudo apt-get upgrade
  sudo apt-get install epiphany-browser cgroup-bin libraspberrypi0 libwayland-client0 libwayland-cursor0 libwayland-server0a


    gdk-pixbuf-query-loaders > /usr/lib/arm-linux-gnueabihf/gdk-pixbuf-2.0/2.10.0/loaders.cache