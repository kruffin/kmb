var io = require('socket.io'),
	connections = [],
	ioServer;

function init (server) {
	ioServer = io.listen(server)

	ioServer.sockets.on('connection', function (socket) {
	connections.push(socket);
	});

	ioServer.sockets.on('disconnect', function (socket) {
		connections.remove(socket);
	});
}

function watch (req, res) {
	var vid = req.body.url || req.query.url;
	if (vid) {
		connections.forEach(function (socket) {
			socket.emit('watch', { url: vid });
		});
		res.send('done');
	} else {
		res.send(400, 'Missing url in body.');
	}
};

 function play (req, res) {
	connections.forEach(function (socket) {
		socket.emit('play');
	});
	res.send('done');
};

function pause (req, res) {
	connections.forEach(function (socket) {
		socket.emit('pause');
	});
	res.send('done');
};

function stop (req, res) {
	res.send(403, 'Not implemented.');
}


module.exports = exports = { watch: watch, play: play, pause: pause, init: init, stop: stop };