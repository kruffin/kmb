var socket = io.connect('http://localhost'),
	vid = document.getElementById('vid');
socket.on('watch', function (data) {
	console.log(data);
	vid.src = data.url;
	vid.load();
	vid.play();
	vid.pause();
});

socket.on('play', function() {
	console.log('playing...');
	vid.play();
});

socket.on('pause', function() {
	console.log('pausing...');
	vid.pause();
});
