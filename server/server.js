var express = require('express'),
	api = require('./omx_api.js');
var app = express(),
	server = require('http').createServer(app),
	port = process.env.PORT || 80;

api.init(server);

server.listen(port);
console.log('Listening on port ' + port);

app.use(express.bodyParser());
app.use(express.static(__dirname + '/public'));

app.post('/watch', api.watch);
app.post('/play', api.play);
app.post('/pause', api.pause);
app.post('/stop', api.stop);



