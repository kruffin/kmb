
var omx = 'omxplayer',
	args = [ '-o', 'hdmi' ],
	childp = require('child_process'),
	player,
	yt = require('./youtube.js');

function init (server) {

};

function watch (req, res) {
	var vid = req.body.url || req.query.url;
	if (vid) {
		if (player) {
			player.stdin.write('q');
		}

		if (vid.indexOf('youtube.com') !== -1) {
			yt.getUrl(vid, function (url) {
				console.log('creating player: ' + url);
				player = createPlayer(url);
				res.send('done');
			});
		} else {
			player = createPlayer(vid);
			res.send('done');
		}
	} else {
		res.send(400, 'Missing url in body.');
	}
};

 function play (req, res) {
 	if (player) {
 		player.stdin.write('p');
 	}
	res.send('done');
};

function pause (req, res) {
	if (player) {
		player.stdin.write('p');
	}
	res.send('done');
};

function stop (req, res) {
	if (player) {
		//player.kill();
		player.stdin.write('q');
	}
	res.send('player stopped.');
}

function createPlayer (video) {
	var argv = args.slice(0, args.length);
	argv.push(video);

	console.log('creating player|', argv);
	var	p = childp.spawn(omx, argv);
	p.on ('close', function () {
		console.log('player closed.');
		player = null;
	});
	p.stderr.on ('data', function (data) {
		console.log(data.toString());
	});
	p.stdout.on ('data', function (data) {
		console.log(data.toString());
	});

	return p;
};


module.exports = exports = { watch: watch, play: play, pause: pause, init: init, stop: stop };
